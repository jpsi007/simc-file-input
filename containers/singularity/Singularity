Bootstrap: docker
From: eicweb.phy.anl.gov:4567/whit/image_recipes/root_base:latest

%help
  Container for file-input simc
  Tools: 
    - simc      : SIMC version that takes file input

%labels
  Maintainer "Sylvester Joosten"
  Version v1.0

%post -c /bin/bash
  echo "Constructing simc container"
  echo "1. ------------------------------------------"
  echo "1. -- Building and installing SIMC"
  echo "1. ------------------------------------------"
  source /usr/local/bin/thisroot.sh
  export PYTHONPATH=/usr/local/lib:$PYTHONPATH
  export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
  cd /tmp
  git clone --recurse-submodules https://eicweb.phy.anl.gov/jlab/simc-file-input.git && \
    mkdir build && cd build && cmake ../simc-file-input && make -j4 install
  cd /tmp && rm -rf build simc-file-input

# ===================================
# GLOBAL
# ===================================

%environment -c /bin/bash
  export PYTHONPATH=/usr/local/lib:$PYTHONPATH
  export PATH=/usr/loca/bin:${PATH}
  export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
  export ROOT_INCLUDE_PATH=/usr/local/include

%runscript
  echo "Launching a shell in the simc container."
  echo "Use through singularity apps recommended (see help for more info)."
  exec bash

# ===================================
# SIMC
# ===================================
%apprun simc
  simc "%@"

%appenv simc
  export PYTHONPATH=/usr/local/lib:$PYTHONPATH
  export PATH=/usr/loca/bin:${PATH}
  export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
  export ROOT_INCLUDE_PATH=/usr/local/include
