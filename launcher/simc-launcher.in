#!/usr/bin/env python3

import argparse
import tempfile
import os
import subprocess

## SIMC enviroment (configured by cmake)
env_path = '@SIMC_DATA_PATH@'
exe_path = '@SIMC_EXE_PATH@'
exe_name = os.path.basename(exe_path)
run_name = 'run_{}'.format(exe_name)
tmp_conf_dir = 'infiles'
tmp_output_dir = 'outfiles'
tmp_worksim_dir = 'worksim'

## actual simc launcher bash script
bash_script='''#!/bin/bash
./simc << eof >> {outdir}/{conf}.log
{conf}
{data}
eof
'''

## parse command line
cmd = argparse.ArgumentParser(
    description='Process the provided data files for a given SIMC configuration.')
cmd.add_argument(
    '--conf', '-c', metavar='input_file', dest='conf', required=True,
    help='SIMC configuration file (inputfile).')
cmd.add_argument(
    '--output', '-o', metavar='output_dir', dest='outdir', required=True,
    help='Output file directory.')
cmd.add_argument(
    'data', metavar='data_file', 
    help='Data file from generator in SIMC text format.')
args=cmd.parse_args()
print('\nRunning SIMC on `{}\''.format(args.data))
print('\nConfiguration: `{}\''.format(args.conf))
print('Output directory: `{}\''.format(args.outdir))

## get absolute path for everything
data_path = os.path.abspath(args.data)
conf_path = os.path.abspath(args.conf)
output_dir = os.path.abspath(args.outdir)

## also get relative paths to be used in our tmp environment
tmp_data_path = os.path.basename(data_path)
tmp_conf_path = os.path.basename(conf_path)
tmp_conf_name, tmp_conf_ext = os.path.splitext(tmp_conf_path)
## make sure our tmp_conf_path ends on '.inp'
if tmp_conf_ext is '.inp':
    pass
else:
    tmp_conf_path = '{}.inp'.format(tmp_conf_name)
## also include our tmp config directory
tmp_conf_path = '{dir}/{file}'.format(dir=tmp_conf_dir, file=tmp_conf_path)

print('Output file base name: `{}\''.format(tmp_conf_name))

print('\nGetting ready to run SIMC...')
## setup a temporary SIMC environment
with tempfile.TemporaryDirectory() as tmpdir:
    print(' - Creating temporary environment...')
    os.chdir(tmpdir)
    for file in os.listdir(env_path):
        os.symlink(env_path + '/' + file, file)

    print(' - Loading SIMC executable...')
    os.symlink(exe_path, exe_name)

    print(' - Loading configuration file...')
    os.mkdir(tmp_conf_dir)
    os.symlink(conf_path, tmp_conf_path)

    print(' - Linking output directories...')
    os.symlink(output_dir, tmp_output_dir)
    os.symlink(output_dir, tmp_worksim_dir)
    # also cleanup logfile if needed
    logfile = '{outdir}/{conf}.log'.format(outdir=output_dir, conf=tmp_conf_name)
    if os.path.exists(logfile):
        os.remove(logfile)

    print(' - Linking input file...')
    os.symlink(data_path, tmp_data_path)


    print(' - Creating run script...')
    launcher = open(run_name, 'w')
    launcher.write(bash_script.format(
        outdir=tmp_output_dir, 
        data=tmp_data_path,
        conf=tmp_conf_name))
    launcher.close()
    print('\nCalling SIMC, good luck!')
    subprocess.call(['sh', run_name])
print('\nConverting output to ROOT')
os.chdir(output_dir)
subprocess.call(['h2root', '{conf}.rzdat'.format(conf=tmp_conf_name)])
print('All done!')
